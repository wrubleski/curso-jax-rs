package rest.exer5;

import java.math.BigDecimal;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

@Path("/funcionario/")
public class RestEmpresa {
	private Funcionario criar() {
		Funcionario contato = new Funcionario();
		contato.setNome("Luiz Henrique");
		contato.setCpf(123123123L);
		contato.setSalario(new BigDecimal("12312.99"));
		return contato;
	}

	@Path("/xml")
	@GET
	@Produces(MediaType.APPLICATION_XML)
	public Funcionario servicoXml() {
		return criar();
	}

	@Path("/json")
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Funcionario servicoJson() {
		return criar();
	}
}
