package rest.exer7;

import java.math.BigDecimal;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class Pessoa {
	private String nome;
	private int idade;
	private BigDecimal salario;

	public Pessoa() {
	};

	public Pessoa(String nome, int idade, BigDecimal salario) {
		super();
		this.nome = nome;
		this.idade = idade;
		this.salario = salario;
	}

	@Override
	public String toString() {
		return "Pessoa [nome=" + nome + ", idade=" + idade + ", salario=" + salario + "]";
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public int getIdade() {
		return idade;
	}

	public void setIdade(int idade) {
		this.idade = idade;
	}

	public BigDecimal getSalario() {
		return salario;
	}

	public void setSalario(BigDecimal salario) {
		this.salario = salario;
	}
}
