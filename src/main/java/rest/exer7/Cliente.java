package rest.exer7;

import java.math.BigDecimal;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.Response;

public class Cliente {
	public static void viaXML() {
		Pessoa fernando = new Pessoa("Fernando", 35, new BigDecimal(1000));

		Client cliente = ClientBuilder.newClient();
		WebTarget web = cliente.target("http://localhost:8080/telefonia/xml");
		Response resposta = web.request().post(Entity.xml(fernando));

		if (resposta.getStatus() == 204) {
			System.out.println("Enviado com sucesso.");
		} else {
			System.out.println("Erro na resposta: " + resposta.toString());
		}

		resposta.close();
		cliente.close();
	}

	public static void viaJSON() {
		Pessoa fernando = new Pessoa("Fernando", 35, new BigDecimal(1000));

		Client cliente = ClientBuilder.newClient();
		WebTarget web = cliente.target("http://localhost:8080/telefonia/json");
		Response resposta = web.request().post(Entity.json(fernando));

		if (resposta.getStatus() == 204) {
			System.out.println("Enviado com sucesso.");
		} else {
			System.out.println("Erro na resposta: " + resposta.toString());
		}

		resposta.close();
		cliente.close();
	}

	public static void main(String[] args) {
		viaXML();
		viaJSON();
	}
}
